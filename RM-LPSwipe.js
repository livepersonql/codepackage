/*vars for swipe*/
var xDown = null;
var yDown = null;

function setupLPEvents() {
  'use strict';

  var ios_message = function () {
    //add message here
    alert('You are in Privacy Mode\nPlease deactivate Privacy Mode and then reload the page to chat.');
    return false;
  }

  try {
    // try to use localStorage
    localStorage.test = 2;
  } catch (e) {
    // there was an error so...
    lpTag.events.bind("LP_OFFERS", "OFFER_IMPRESSION", function (eventData, eventInfo) {
      try {
        var clicks = document.querySelectorAll('[data-lp-event=click]');

        for (var i = 0; i < clicks.length; i++) {
          clicks[i].onclick = ios_message;
        }
      } catch (e) {
        _log(e);
      }
    });
  }

  lpTag.events.bind("LP_OFFERS", "OFFER_IMPRESSION", function (eventData, eventInfo) {
    if (eventData.engagementType == 5) {
      _log("embedded button shown");
      _log(eventData.engagementName);

      //use this statement to modify the Rocket Button Button
      if (eventData.engagementName.indexOf("RM - Talk to Us Button") !== -1) {
        var button = document.getElementById("lpButtonDiv");

        if (button.childNodes.length !== 0) {
          var firstNode = button.childNodes[0];

          if (firstNode.className.indexOf("LPMcontainer") === -1) {
            button.removeChild(firstNode);
          }
        }
      }
    }
  });

  window.lpTag.events.bind('lpUnifiedWindow', 'state', function (eventData) {
    _log(eventData.state);
    if (eventData.state === 'preChat') {
      _log('preChat matched');

      if (window.lpTag.hideSurvey) {
        surveyAvailable();
      }

      setTimeout(function () {
        var allQuestions = document.getElementsByClassName('lp_select_field');
        var routingDropDownNumber = allQuestions.length - 1;
        var radioButtonQuestionArray = document.getElementsByClassName('lp_radio_button');

        // Find and hide routing question dropdown
        var routingQuestion = allQuestions[routingDropDownNumber];
        var routingID = routingQuestion.getAttribute("aria-labelledby");
        var routingLabel = document.getElementById(routingID);
        var isMobile = document.getElementsByClassName('lp_mobile').length > 0;
        var isTablet = document.getElementsByClassName('lp_tablet').length > 0;

        if (isMobile || isTablet) {
            routingLabel.innerHTML = "Almost done, just click the submit button below!";
        } else {
            routingLabel.setAttribute("style", "display: none !important;");
        }
        routingQuestion.setAttribute("style", "display: none !important;");
        _log('routingQuestion bound and select box is invisible');

        // find real submit button and create fake submit button
        /*
        var realSubmitButton = document.querySelectorAll('button[data-lp-point="submit_button"]')[0];
        if (typeof(realSubmitButton)==="undefined") {
          _log("realSubmitButton not found");
          return;
        } else {
          _log("realSubmitButton bound");
        }

        var lpButtonsArea = realSubmitButton.parentNode;
        _log('lpButtonsArea bound');

        var fakeSubmitButton = realSubmitButton.cloneNode(true);
        lpButtonsArea.appendChild(fakeSubmitButton);
        fakeSubmitButton.id = 'fakeSubmitButton';
        _log('fakeSubmitButton = ' + fakeSubmitButton);

        realSubmitButton.setAttribute("style", "display: none !important;");
        _log("real button hidden");

        // if the next button is hidden, append the fake submit button, else append the fake button only when the submit button is not hidden
        var nextButton = document.querySelectorAll('button[data-lp-point="next_button"]')[0];
        var previousButton = document.querySelectorAll("button[data-lp-point='prev_button']")[0];

        if (typeof(previousButton) === "undefined") {
          _log("previousButton not found");
          return;
        } else {
          previousButton.addEventListener("click", function () {
            if (realSubmitButton.className.indexOf("lpHide") === -1) {
              fakeSubmitButton.className += " lpHide";
              _log("hideSubmit");
            }
          });
        }

        if (typeof(nextButton) === "undefined") {
          _log("nextButton not found");
          return;
        } else {
          if (nextButton.className.indexOf("lpHide") !== -1) {
            lpButtonsArea.appendChild(fakeSubmitButton);
            _log("fake submit button appended");
          } else {
            nextButton.addEventListener("click", function () {
              setTimeout(function () {
                try {
                  if (realSubmitButton.className.indexOf("lpHide") !== -1) {
                    _log("do not append fake submit button");
                  } else {
                    fakeSubmitButton.className = fakeSubmitButton.className.replace(" lpHide", "");
                    lpButtonsArea.appendChild(fakeSubmitButton);
                    _log("fake submit button appended");

                    if (isMobile || isTablet) {
                      fakeSubmitButton.click();
                      _log("fake submit button clicked to hide blank page");
                    }
                  }
                } catch (e) {
                  _log(e);
                }
              }, 500);
            });

            var pagesArea = document.getElementsByClassName('lp_pages_area')[0];

            pagesArea.addEventListener('touchstart', function(evt) {
              xDown = evt.touches[0].clientX;
              yDown = evt.touches[0].clientY;
            });

            pagesArea.addEventListener('touchmove', function(evt) {
              if (!xDown || !yDown) {
                return;
              }

              var xCurr = evt.touches[0].clientX;
              var yCurr = evt.touches[0].clientY;
              var xDiff = xDown - xCurr;
              var yDiff = yDown - yCurr;

              if (Math.abs(xDiff) + Math.abs(yDiff) < 150) {
                //don't do anything here until we have at least 150 pixels of distance
                return;
              }

              if (Math.abs(xDiff) < Math.abs(yDiff)) {
                //we don't care about vertical swipes. reset the origin values.
                xDown = null;
                yDown = null;
                return;
              }

              if (xDiff > 0) {
                //swipe left
                setTimeout(function() {
                  try {
                    if (realSubmitButton.className.indexOf("lpHide") !== -1) {
                      _log("do not append fake submit button");
                    } else {
                      fakeSubmitButton.className = fakeSubmitButton.className.replace(" lpHide","");
                      lpButtonsArea.appendChild(fakeSubmitButton);
                      _log("fake submit button appended");

                      if (isMobile || isTablet) {
                        fakeSubmitButton.click();
                        _log("fake submit button clicked to hide blank page");
                      }
                    }
                  } catch (e) {
                    _log(e);
                  }
                }, 500);

              } else {
                //right swipe
                if (realSubmitButton.className.indexOf("lpHide") === -1) {
                  fakeSubmitButton.className += " lpHide";
                  _log("hideSubmit");
                }
              }

              //done swiping, reset origin values
              xDown = null;
              yDown = null;
            });
          }
        }

        //when the client clicks the fake submit button, perform the logic to get the chat rep skill
        fakeSubmitButton.addEventListener('click', function () {
          _log('fakeSubmitButton clicked');

          var stateQuestion = allQuestions[0];
          var loanQuestion = radioButtonQuestionArray;
          var stateAnswer;
          var loanAnswer;

          //grab the loan purpose
          for (var i = 0; i < loanQuestion.length; i++) {
            if (loanQuestion[i].childNodes[0].checked) {
              loanAnswer = loanQuestion[i].childNodes[1].innerHTML;
            }
          }

          // Dispatch and advance to chat
          realSubmitButton.click();
          _log('real submit button clicked');
        });

        //if we know the relevant points of client info, we don't really hide chat.
        //  We simply fill in the known fields for the client, then click the submit button for them. tricky tricky!
        var changeEvent = new LP_CustomEvent('change');
        var clickEvent = new LP_CustomEvent('input');

        var personalInfo = lpTag.sdes.get().personal[lpTag.sdes.get().personal.length - 1].personal;
        var customerInfo = lpTag.sdes.get().ctmrinfo[lpTag.sdes.get().ctmrinfo.length - 1].info;

        var textFieldQuestions = document.getElementsByClassName('lp_input-field');
        var nameQS = textFieldQuestions[0];
        var emailQS = textFieldQuestions[1];
        var stateQS = allQuestions[0];
        var purposeQS = radioButtonQuestionArray;

        var firstname = normalizeLPField(personalInfo['firstname']);
        var lastname = normalizeLPField(personalInfo['lastname']);
        var state = normalizeLPField(customerInfo['accountName']);
        var email = normalizeLPField(personalInfo['contacts'][0]['email']);
        var purpose = normalizeLPField(customerInfo['ctype']);

        var identifier = firstname + ' ' + lastname;
        if (firstname === '' || lastname === '') {
            identifier = firstname + lastname;
        }

        nameQS.value = identifier;
        nameQS.dispatchEvent(clickEvent);

        emailQS.value = email;
        emailQS.dispatchEvent(clickEvent);

        for(var i = 0; i < stateQS.options.length; i++) {
            if (stateQS.options[i].text === state ) {
                stateQS.selectedIndex = i;
                break;
            }
        }
        stateQS.dispatchEvent(changeEvent);

        for (var i = 0; i < purposeQS.length; i++) {
          if (purposeQS[i].childNodes[1].innerHTML.toUpperCase() === purpose.toUpperCase()) {
            purposeQS[i].childNodes[0].checked = true;
            purposeQS[i].childNodes[0].click();
          }
        }

        if (window.lpTag.hideSurvey) {
          fakeSubmitButton.click();
        }
        */
      }, 500);
    }
  });
}
