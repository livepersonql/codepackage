var counter = 0;
var postChat = false;
lpTag.events.bind("lpUnifiedWindow", "state", function(eventData, eventInfo) {
//   console.log("Data: ", eventData.state);
   //console.log("Info: ",eventInfo);

   closeButton = document.getElementsByClassName("lp_close")[0];

// If a chat is started reset variables on page
   if (eventData.state === "initialised") {
    //   console.log("reset counter");
       counter = 0;
       postChat = false;
   }
   // If post chat survey is shown, flag to not close the window
   if (eventData.state === "postChat") {
   //    console.log(1);
       postChat = true;
   }
   // else check to see if post chat is completed, or if the chat has ended
   else {
       // if the post chat is completed then close chat window
       if (eventData.state === "applicationEnded") {
           closeButton.click();
       //    console.log(234);
       }
       // else see if chat has ended
       else {
           // if chat has ended, increase the counter and run the check for post chat survey
           if (eventData.state === "ended") {
               counter++;
               // only run this function once
               if (counter == 1) {
               //    console.log(2);
                   setTimeout(function() {
                    //   console.log(3);
                       // if there is no flag for post chat survey, close the window
                       if (postChat == false) {
                           closeButton.click();
                 //          console.log(4);
                       }
                       //lp_cancel_button
                         //lp_cancel_button
                       cancelButton = document.getElementsByClassName("lp_cancel_button")[0] 
                       cancelButton["onclick"] = function(){
                           closeButton.click();
                       };
                   }, 500);
               }
           }
       }
   }
})