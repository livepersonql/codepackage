var LPCustomerInfo = {
	"type": "ctmrinfo",  //MANDATORY customer info
		"info": {
		"ctype": typeof custType === 'undefined'?"":custType,
		"accountName": typeof acctName === 'undefined'?"":acctName,

	}
};

var LPPersonalInfo = {
	"type": "personal",  
	"personal": {
		"firstname": typeof personalFirstName === 'undefined'?"":personalFirstName,
		"lastname": typeof personalLastName === 'undefined'?"":personalLastName,
		"age": {
		   "age": typeof personalAge === 'undefined'?"":personalAge
		   },
		"contacts": {
		   "email": typeof personalEmail === 'undefined'?"":personalEmail,
       "phone": typeof personalPhone === 'undefined'?"":personalPhone
		   }
    }
};



function sendSDES(obj) {
   for(var prop in obj) {

   //   console.log("prop="+prop);
      if (!obj[prop]) {obj[prop] = "";}
     // console.log(obj[prop]);
      if (obj[prop].length != 0 && obj[prop] != null) {
         if (typeof obj[prop] === "object") {
         //item needs sub objects iterated through
          //  console.log(obj[prop]);
            sendSDES(obj[prop]);
         } else {
            //item has value and doesn't need anything done to it
         //   console.log(obj[prop]);
         }
     } else {
      //check if new obj[prop] children are blank (deleted values)


      //then wipe that prop
         delete obj[prop];
     }
   }

lpTag.sdes.push(obj);
}