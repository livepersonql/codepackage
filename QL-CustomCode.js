/**
 * Created by gperez@liveperson.com on 11/21/17.
 */

// BEGIN QL.COM CUSTOM CODE
function fixButtonStyle(lpmContainer)
{
    var link;

    if (typeof lpmContainer.parentElement !== 'object') {
        console.log('leaving prematurely 2.')
        return;
    }

    // Do NOT modify any chat elements besides  these.
    if (lpmContainer.parentElement.id !== 'lpButtonDiv'
        && lpmContainer.parentElement.id !== 'lpButtonDiv2'
        && lpmContainer.parentElement.id !== 'lpButtonDiv3'
        && lpmContainer.parentElement.id !== 'lpButtonDiv4'
        && lpmContainer.parentElement.id !== 'lpButtonDiv5'
        && lpmContainer.parentElement.id !== 'LPButtonDiv06'
        && lpmContainer.parentElement.id !== 'lpButton6'
        && lpmContainer.parentElement.id !== 'lpButton7'
        && lpmContainer.parentElement.id !== 'lpButtonMobile'
        && lpmContainer.parentElement.id !== 'lpButtonHeaderNew'
        && lpmContainer.parentElement.id !== 'lpButtonFooterNew'
        && lpmContainer.parentElement.id !== 'lpButtonSlimHeaderNew'
        && lpmContainer.parentElement.id !== 'lpButtonMobileHeaderNew'
        && lpmContainer.parentElement.id !== 'LPButtonQLCTANew'){
        return;
    }

    lpmContainer.setAttribute('style', 'display: inline;');
    lpmContainer.setAttribute('tabIndex', '-1');

    link = lpmContainer.querySelector('a');

    if (typeof link === 'object') {
        link.className += ' link-plain';
    }

    if (lpmContainer.parentElement.id === 'lpButton7') {
        link.className += ' link-disclaimer';
    }
}

// Pass configuration and lead data.
if (typeof window['liveEngageData'] === 'object') {
    // Patch phone number format
    if (liveEngageData.personal.personal.hasOwnProperty('contacts')) {
        var phoneNo = liveEngageData.personal.personal.contacts[0].phone.AreaCode
            + '-' + liveEngageData.personal.personal.contacts[0].phone.Prefix
            + '-' + liveEngageData.personal.personal.contacts[0].phone.Suffix;
        liveEngageData.personal.personal.contacts[0].phone = phoneNo;
    }

    // Patch in loan purpose and rout
    if (liveEngageData.ctmrinfo.info.cstatus === "true") {
        liveEngageData.ctmrinfo.info.accountName = liveEngageData.info.accountName;
        liveEngageData.ctmrinfo.info.ctype = liveEngageData.info.ctype;
    }

    for (var data in liveEngageData) {
        // skip inherited from the prototype chain.
        if (!liveEngageData.hasOwnProperty(data)) {
            continue;
        }

        lpTag.sdes.push(liveEngageData[data]);
    }
}

// Modify the chat links
lpTag.events.bind("LP_OFFERS", "OFFER_IMPRESSION", function(eventData) {
    var lpmContainers, lpmContainer;

    if (eventData.engagementType === 5) {
        // Get the div with the styles on it.
        lpmContainers = document.querySelectorAll('.LPMcontainer');

        // Remove the styles from the DIVs.
        if (typeof lpmContainers === 'object') {
            for (var i = 0; i < lpmContainers.length; i++) {
                lpmContainer = lpmContainers[i];
                if (typeof lpmContainer === 'object') {
                    fixButtonStyle(lpmContainer);
                }
            }
        }

        lpmContainer = null;
        lpmContainers = null;
    }
});
// END QL.COM CUSTOM CODE


// BEGIN iOS PRIVATE BROWSING MODE PATCH
var ios_message = function () {
    //add message here
    alert('You are in Privacy Mode\nPlease deactivate Privacy Mode and then reload the page to chat.');
    return false;
}

try {
    // try to use localStorage
    localStorage.test = 2;
} catch (e) {
    // there was an error so...
    lpTag.events.bind("LP_OFFERS", "OFFER_IMPRESSION", function(eventData, eventInfo){
        try {
            var clicks = document.querySelectorAll('[data-lp-event=click]');
            for (var i = 0; i < clicks.length; i++) {
                var newclick = clicks[i].cloneNode(true);
                clicks[i].parentNode.replaceChild(newclick, clicks[i]);
                newclick.onclick = ios_message;
            }
        }
        catch(e){
            console.log(e)
        };
    });
}
// END iOS PRIVATE BROWSING MODE PATCH