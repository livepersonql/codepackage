This is a sample code package for Live Engage.  It contains:

LEdeployment-example.html -- The LEdeployment-example.html file contains code that should be added to aall tagged pages.  It sets Engagement Attributes and sends to Live Person.  Engagements Attributes not needed for current page may be removed.  

Note:  The following 4 files are referenced by LEdeployment-example.html.  If not in the same directory, then the reference must include directory path

le-mtagconfig.js -- JavaScript file which communicates with Live Person.  Must be hosted on client's server. No changes are required to this file.

LPAttributes -- Engagement Attributes JSON samples. Must be hosted on client's server. No changes required initially.  Attributes not currently used are commented for future availablity.

LPCloseWindow.js -- optional, remove reference in le2-mtagconfig.js file if not needed.  Close embedded chat window after chat ends

LPSurveys.js -- contains code to copy pre-chat survey answers to offline survey
