//BEING QL.COM CUSTOM CODE
function fixButtonStyle(lpmContainer)
{
    var link;

    if (typeof lpmContainer.parentElement !== 'object'){
        console.log('leaving prematurely 2.');
        return ;
    }

    if(lpmContainer.parentElement.id !== 'lpButtonDiv'
        && lpmContainer.parentElement.id !== 'lpButtonDiv2'
        && lpmContainer.parentElement.id !== 'lpButtonDiv3'
        && lpmContainer.parentElement.id !== 'lpButtonDiv4'
        && lpmContainer.parentElement.id !== 'lpButtonDiv5'
        && lpmContainer.parentElement.id !== 'lpButtonDiv6'
        && lpmContainer.parentElement.id !== 'lpButton6'
        && lpmContainer.parentElement.id !== 'lpButton7'
        && lpmContainer.parentElement.id !== 'lpButtonMobile'
        && lpmContainer.parentElement.id !== 'lpButtonHeaderNew'
        && lpmContainer.parentElement.id !== 'lpButtonFooterNew'
        && lpmContainer.parentElement.id !== 'lpButtonSlimHeaderNew'
        && lpmContainer.parentElement.id !== 'lpButtonMobileHeaderNew'
        && lpmContainer.parentElement.id !== 'LPButtonQLCTANew'){

        return;
    }

    lpmContainer.setAttribute('style','display: inline');
    lpmContainer.setAttribute('tabIndex','-1');

    link  = lpmContainer.querySelector('a');

    if(typeof link === 'object'){
        link.className += ' link-plain';
    }

    if(lpmContainer.parentElement.id === 'lpButton7'){
        link.className += ' link-disclaimer';
    }

}

var le_debug=false;

/*livePerson debug messages: set lp_debug = true on page*/
function _log (msg,args) {

    if(le_debug) {
        var output = "";
        if(typeof(args) !== "undefined" && args.length > 0) {
            for (var i = 0; i < args.length; i++) {
                output += " | "+JSON.stringify(args[i]);
            };
        }
        console.log('~~~ LivePerson PCS Routing -> ',msg,output);
    }
}

/*vars for swipe*/
var xDown = null;
var yDown = null;

lpTag.events.bind("lpUnifiedWindow", "state", function(eventData, eventInfo){

    _log(eventData.state);
    if (eventData.state == "preChat") {
        _log("preChat matched");
        setTimeout(function(){
            // find real submit button and create fake submit button
            var realSubmitButton = document.querySelectorAll("button[data-lp-point='submit_button']")[0];
            if (typeof(realSubmitButton)==="undefined") {
                _log("realSubmitButton not found");
                return;
            } else {
                _log("realSubmitButton bound");
            }
            var lpButtonsArea = realSubmitButton.parentNode;
            var fakeSubmitButton = document.getElementById('fakeSubmitButton');

            // if the next button is hidden, append the fake submit button, else append the fake button only when the submit button is not hidden
            var nextButton = document.querySelectorAll("button[data-lp-point='next_button']")[0];
            var previousButton = document.querySelectorAll("button[data-lp-point='prev_button']")[0];
            if (typeof(previousButton)==="undefined") {
                _log("previousButton not found");
                return;
            }
            else {
                previousButton.addEventListener("click", function() {
                    setTimeout(function() {
                        if (realSubmitButton.className.indexOf("lpHide") !== -1) {
                            if (fakeSubmitButton.className.indexOf("lpHide") === -1) {
                                fakeSubmitButton.className += " lpHide";
                                _log("hideSubmit");
                            }
                        }
                    }, 500);
                });

            }
            if (typeof(nextButton)==="undefined") {
                _log("nextButton not found");
                return;
            }
            else {
                if (nextButton.className.indexOf("lpHide") !== -1) {
                    lpButtonsArea.appendChild(fakeSubmitButton);
                    _log("fake submit button appended");
                }
                else {
                    nextButton.addEventListener("click", function(){
                        setTimeout(function() {
                            try {
                                if (realSubmitButton.className.indexOf("lpHide") !== -1) {
                                    _log("do not append fake submit button");
                                } else {
                                    fakeSubmitButton.className = fakeSubmitButton.className.replace(" lpHide","");
                                    lpButtonsArea.appendChild(fakeSubmitButton);
                                    _log("fake submit button appended");
                                }
                            } catch (e) {console.log(e)}
                        }, 500);
                    });

                    var pagesArea = document.getElementsByClassName('lp_pages_area')[0];
                    pagesArea.addEventListener('touchstart', function(evt) {
                        xDown = evt.touches[0].clientX;
                        yDown = evt.touches[0].clientY;
                    });
                    pagesArea.addEventListener('touchmove', function(evt) {
                        if ( ! xDown || ! yDown ) {
                            return;
                        }

                        var xUp = evt.touches[0].clientX;
                        var yUp = evt.touches[0].clientY;

                        var xDiff = xDown - xUp;
                        var yDiff = yDown - yUp;


                        if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
                            _log('left swipe');
                            if ( xDiff > 0 ) {/* left swipe */
                                setTimeout(function() {
                                    try {
                                        if (realSubmitButton.className.indexOf("lpHide") !== -1) {
                                            _log("do not append fake submit button");
                                        } else {
                                            fakeSubmitButton.className = fakeSubmitButton.className.replace(" lpHide","");
                                            lpButtonsArea.appendChild(fakeSubmitButton);
                                            _log("fake submit button appended");
                                        }
                                    } catch (e) {console.log(e)}
                                }, 500);
                            }
                            else {/* right swipe */
                                //var realSubmitButton = document.querySelectorAll("button[data-lp-point='submit_button']")[0];
                                //var fakeSubmitButton = document.getElementById('fakeSubmitButton');
                                _log('right swipe');
                                setTimeout(function() {
                                    if (realSubmitButton.className.indexOf("lpHide") !== -1) {
                                        if (fakeSubmitButton.className.indexOf("lpHide") === -1) {
                                            fakeSubmitButton.className += " lpHide";
                                            _log("hideSubmit");
                                        }
                                    }
                                }, 500);
                            }
                        }
                        /* reset values */
                        xDown = null;
                        yDown = null;



                    });
                }
            }
        }, 1000);
    }

});
// END DO NOT MODIFY


