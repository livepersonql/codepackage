var PCSName = "";
var PCSEmail = "";
var PCSLocation = "";
var PCSState = -1;
var PCSStateValue = "";
var PCSPurpose = ""
PCSPhone = "";
var updated = 0;
globalPersonalInfo = "";
questions = "";
var  autoSubmit = "false";
journeyState = "";

PCSexists = false;

var   stateQS = "";
var      purposeQS = [];
var      nameQS = "";
var      emailQS = "";
var       phoneQS = "";
var preChat = false;

var realSubmitButton = "";
var fakeSubmitButton = "";

console.log("Version 47 -- unhide questions, Uppercase purpose, fix fake submit");
var codeVersion = 'Version 47 -- unhide questions, Uppercase purpose, fix fake submit';                                 //added 28-11-2017

function getVersionCode(){
    return codeVersion;                                                                                                 //added 28-11-2017
}

var globalContacts = [];


lpTag.events.bind("lpUnifiedWindow", "state", function(eventData, eventInfo) {
    console.log("Data: ", eventData.state);

    if (eventData.state === "preChat") {
        preChat = true;
        journeyState = "preChat"
    }
    if (eventData.state === "offline") {

        journeyState = "offline";
        console.log("Offline survey");

        setTimeout(function() {
            var buttonsLength = document.querySelectorAll("button[data-lp-point='submit_button']").length;
            console.log("ButtonLength="+buttonsLength);
            //        if (preChat) {
            //             offlineSubmitButton = document.querySelectorAll("button[data-lp-point='submit_button']")[buttonsLength-1];
            //           console.log("Prechat exists");
            //      }
            //      else {
            //           offlineSubmitButton = document.querySelectorAll("button[data-lp-point='submit_button']")[0];;
            //            console.log("No prechat");
            //      }
            offlineSubmitButton = document.querySelectorAll("button[data-lp-point='submit_button']")[buttonsLength-1];
            console.log("OfflineSubmit="+offlineSubmitButton);
            //document.querySelectorAll("button[data-lp-point='submit_button']")[0].onclick = function() {
            offlineSubmitButton["onclick"] = function() {
                console.log("Offline submitted");
                createLead();
            };
        }, 800);

        console.log("Offline button set");
    }

})


function setPCSSubmit(){
    console.log("setPCSSubmit");
    if (document.getElementsByClassName("lp_submit_button").length > 0){
        submitButton = document.getElementsByClassName("lp_submit_button")[0];
        console.log("SB="+submitButton);
        submitButton["onclick"] = function(){

            // console.log("GetPCS");
            getPCSData();
        };
        console.log("Set submit");
        PCSexists = true;
        clearTimeout(setPCSSubmit);
    } else   {

        setTimeout(setPCSSubmit,600);

    }
}

/*
 function setOfflineSubmit(){
 console.log("setOfflineSubmit");
 if (document.getElementsByClassName("lp_submit_button").length > 0){
 submitButton = document.getElementsByClassName("lp_submit_button")[0];
 console.log("SB="+submitButton);
 submitButton["onclick"] = function(){

 // console.log("GetPCS");
 getPCSData();
 };
 console.log("Set submit");
 PCSexists = true;
 clearTimeout(setPCSSubmit);
 } else {
 setTimeout(setPCSSubmit,150);

 }
 }
 */

/*
 <button class="lp_close_survey_button lpHide" data-lp-point="close_survey_button" data-lp-cust-id="close_survey_button" style="background-color:#006838;color:#FFFFFF">Close</button>
 */



function getPCSData(){
    console.log("GetPCS");

    PCSName = nameQS.value;
    console.log("Name="+PCSName);
    PCSEmail = emailQS.value;
    console.log("Email="+PCSEmail);
    PCSState = stateQS[stateQS.selectedIndex].text;
    PCSPurpose = purposeQS[purposeQS.selectedIndex].text;
    PCSPhone = phoneQS.value;

}



/*custom event for change and input events*/
function lp_CustomEvent ( event, params ) {
    params = params || { bubbles: false, cancelable: false, detail: undefined };
    var evt = document.createEvent( 'CustomEvent' );
    evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
    return evt;
}

lp_CustomEvent.prototype = window.Event.prototype;

window.lp_CustomEvent = lp_CustomEvent;



/*to translate abbreviation of state to full name*/
function abbrState(input, to){

    var states = [
        ['Arizona', 'AZ'],
        ['Alabama', 'AL'],
        ['Alaska', 'AK'],
        ['Arizona', 'AZ'],
        ['Arkansas', 'AR'],
        ['California', 'CA'],
        ['Colorado', 'CO'],
        ['Connecticut', 'CT'],
        ['Delaware', 'DE'],
        ['Florida', 'FL'],
        ['Georgia', 'GA'],
        ['Hawaii', 'HI'],
        ['Idaho', 'ID'],
        ['Illinois', 'IL'],
        ['Indiana', 'IN'],
        ['Iowa', 'IA'],
        ['Kansas', 'KS'],
        ['Kentucky', 'KY'],
        ['Kentucky', 'KY'],
        ['Louisiana', 'LA'],
        ['Maine', 'ME'],
        ['Maryland', 'MD'],
        ['Massachusetts', 'MA'],
        ['Michigan', 'MI'],
        ['Minnesota', 'MN'],
        ['Mississippi', 'MS'],
        ['Missouri', 'MO'],
        ['Montana', 'MT'],
        ['Nebraska', 'NE'],
        ['Nevada', 'NV'],
        ['New Hampshire', 'NH'],
        ['New Jersey', 'NJ'],
        ['New Mexico', 'NM'],
        ['New York', 'NY'],
        ['North Carolina', 'NC'],
        ['North Dakota', 'ND'],
        ['Ohio', 'OH'],
        ['Oklahoma', 'OK'],
        ['Oregon', 'OR'],
        ['Pennsylvania', 'PA'],
        ['Rhode Island', 'RI'],
        ['South Carolina', 'SC'],
        ['South Dakota', 'SD'],
        ['Tennessee', 'TN'],
        ['Texas', 'TX'],
        ['Utah', 'UT'],
        ['Vermont', 'VT'],
        ['Virginia', 'VA'],
        ['Washington', 'WA'],
        ['Washington Dc', 'DC'],
        ['West Virginia', 'WV'],
        ['Wisconsin', 'WI'],
        ['Wyoming', 'WY'],
    ];

    if (to == 'abbr'){
        input = input.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        for(i = 0; i < states.length; i++){
            if(states[i][0] == input){
                return(states[i][1]);
            }
        }
    } else if (to == 'name'){
        input = input.toUpperCase();
        for(i = 0; i < states.length; i++){
            if(states[i][1] == input){
                return(states[i][0]);
            }
        }
    }
}




// Pre-set PCS from SDEs
//	 setTimeout(function( {prefillPCS();} ), 1500);

function fillPCS(){
    //  console.log("fillPCS");
    if (document.getElementsByClassName("lp_submit_button").length > 0){
        console.log("Sub button");
        prefillPCS();
        console.log("Set  PCS");
        clearTimeout(fillPCS);
    } else  {
        setTimeout(fillPCS,600);

    }
    //  }
}


fillPCS();



function prefillPCS() {
    //      console.log("prefillPCS");

    //    console.log("Journey="+journeyState);


    if (journeyState === "preChat") {
        setPCSSubmit();
    }

    if (journeyState != "preChat") {
        console.log("preFillPCS -- offline - returning");
        //    setOfflineSubmit();
        return;
    }


    var allQuestions = document.getElementsByClassName('lp_select_field');
    var routingDropDownNumber = allQuestions.length-1;
    var textFieldQuestions = document.getElementsByClassName("lp_input-field");
    var radioButtonQuestionArray = document.getElementsByClassName('lp_radiobutton_wrapper');
    var routingQuestion = allQuestions[routingDropDownNumber];
    var textFieldRequiredQuestions = [];
    //    console.log("Qlength="+textFieldQuestions.length);
    for(var i = 0; i < textFieldQuestions.length;i++) {
        //              console.log("aria="+textFieldQuestions[i].getAttribute("aria-required"));
        if(textFieldQuestions[i].getAttribute("aria-required") === "true"){

            textFieldRequiredQuestions.push(textFieldQuestions[i]);
        }
    }
    // Find and hide questions based on div put in label
    var hide_fields = document.getElementsByClassName('hide_field_q');

    for(var i = 0;i < hide_fields.length; i++) {
        hide_fields[i].parentNode.parentNode.childNodes[1].setAttribute("style", "display: none !important;");
    }

    var hide_all = document.getElementsByClassName('hide_both_q');

    for(var i = 0;i < hide_all.length; i++) {
        hide_all[i].parentNode.parentNode.childNodes[0].setAttribute("style", "display: none !important;");
        hide_all[i].parentNode.parentNode.childNodes[1].setAttribute("style", "display: none !important;");
    }
    console.log("Questions hidden");

    // find real submit button and create fake submit button
    realSubmitButton = document.querySelectorAll("button[data-lp-point='submit_button']")[0];
    if (typeof(realSubmitButton)==="undefined") {
        console.log("realSubmitButton not found");
        return;
    } else {
        console.log("realSubmitButton found");
    }
    var lpButtonsArea = realSubmitButton.parentNode;
    console.log("lpButtonsArea bound");

    //   fakeSubmitButton = realSubmitButton.cloneNode(true);
    //   lpButtonsArea.appendChild(fakeSubmitButton);
    //  fakeSubmitButton.id = "fakeSubmitButton";
    //   console.log("fakeSubmitButton = " + fakeSubmitButton);

    //   realSubmitButton.setAttribute("style", "display: none !important;");





    if(true) {   // Unconditional

        /*change events to elements in PCS*/
        var changeEvent = new lp_CustomEvent('change');
        var clickEvent = new lp_CustomEvent('input');

        var sdes = lpTag.sdes.get();
        var cinfo = sdes.ctmrinfo;
        var customerInfo = [];
        if(cinfo !== undefined && cinfo[cinfo.length-1].info !== undefined) {
            customerInfo = lpTag.sdes.get().ctmrinfo[lpTag.sdes.get().ctmrinfo.length - 1].info;

        }
        else {
            console.log('customer info not set');
        }

        /*get question values from SDES on page*/
        var sdes = lpTag.sdes.get();
        //   var cinfo = sdes.ctmrinfo;
        var pinfo = sdes.personal;
        autoSubmit = customerInfo.cstatus;
        console.log("Autosubmit="+autoSubmit);
        var personalInfo = [];


        if(pinfo !== undefined && pinfo[pinfo.length-1].personal !== undefined) {
            personalInfo = lpTag.sdes.get().personal[lpTag.sdes.get().personal.length - 1].personal;
        }
        else {
            console.log('personal info not set');
        }



        /*get questions from pcs*/
        stateQS = allQuestions[0];
        //    purposeQS = radioButtonQuestionArray[0].childNodes;
        purposeQS = allQuestions[1];
        nameQS = textFieldRequiredQuestions[0];
        emailQS = textFieldRequiredQuestions[1];
        console.log("Email="+textFieldRequiredQuestions[1]);
        phoneQS = textFieldRequiredQuestions[2];

        var contactsArr = personalInfo.contacts;

        globalContacts = personalInfo.contacts;

        if(contactsArr === undefined) {
            contactsArr = [];
        }

        // var contacts = contactsArr[0];
        var contacts = globalContacts;
        console.log("Contacts="+contacts);
        if(contacts === undefined) {
            contacts = [];
        }

        /*get question answers from sdes on page*/
        var firstname = personalInfo.firstname;
        var lastname = personalInfo.lastname;
        var state = customerInfo.accountName;
        //    var email = contacts.email;

        // var phone = personalInfo.contacts.phone; //Accessing Phone 2017-11-28
        // var email = personalInfo.contacts.email; //accessing phone 2017-11-28
        var email = personalInfo.contacts[0].email; // New Access working
        var phone = personalInfo.contacts[0].phone;  // New Access working
        var purpose = customerInfo.ctype.charAt(0).toUpperCase() + customerInfo.ctype.slice(1);

        /*type checking answers*/
        if(firstname === undefined) {
            firstname = "";

        }
        if(lastname === undefined) {
            lastname = "";

        }
        if(state === undefined) {
            state = "";
        }
        if(email === undefined) {
            email = "";
        }
        if(phone === undefined) {
            phone = "";
        }
        if(purpose === undefined) {
            purpose = "";
        }

        if(typeof state !== "undefined" && state.length === 2) {
            state = abbrState(state, "name");
        }

        //make identifier answer
        var identifier = firstname + " " + lastname;

        /*auto fill questions with answers
         fire events for LE to recognize change
         */
        nameQS.value = identifier;
        nameQS.dispatchEvent(clickEvent);

        emailQS.value = email;
        emailQS.dispatchEvent(clickEvent);

        console.log("phone="+phone);
        if (phone) {
            phoneQS.value = phone.replace(/\D/g,'');
            phoneQS.dispatchEvent(clickEvent);
        }

        var desiredState = state;
        for(var i=0; i<stateQS.options.length; i++) {
            if (stateQS.options[i].text == desiredState ) {
                stateQS.selectedIndex = i;
                break;
            }
        }
        stateQS.dispatchEvent(changeEvent);

        var desiredPurpose = purpose;
        for(var i=0; i<purposeQS.options.length; i++) {
            if (purposeQS.options[i].text == desiredPurpose ) {
                purposeQS.selectedIndex = i;
                break;
            }
        }
        purposeQS.dispatchEvent(changeEvent);



        /*checking if all passed
         Case 1) Questions not found
         Case 2) All sdes were not found to auto submit survey, re-show survey
         Case 3) Auto submit PCS
         */
        if(stateQS === "" || purposeQS.length === 0 || nameQS === "" || emailQS === "" || phone === "") {
            console.log('questions not found');
        }
        else if (state === "" || email === ""  || purpose === "" || identifier === ""){
            document.getElementsByClassName('lp_survey_area')[0].setAttribute("style", "display: block !important;")
        }else {
            console.log("PCS submitting");
            if(autoSubmit === "true") {
                // realSubmitButton.click();
                setTimeout(document.getElementById("fakeSubmitButton").click(),2000);
            }
        }
    }

}

//});


//    Prefill Offline survey

function fillOffline(){
    //  console.log("fillOffline");
    if ((document.getElementsByClassName("lp_submit_button").length > 0) &&
        (journeyState === "offline")) {
        console.log("PFPCS Clicked");
        prefillOffline();
        console.log("Set  PCS");
        clearTimeout(fillOffline);
    } else  {
        setTimeout(fillOffline,600);

    }
    //  }
}


fillOffline();


function prefillOffline() {
    console.log("prefillOffline");


    if (!PCSexists) {
        console.log("No PCS");

        //    return;
    }

    if (journeyState != "offline") {
        console.log("Not offline");
        return;
    }


    var allQuestions = document.getElementsByClassName('lp_select_field');
    var routingDropDownNumber = allQuestions.length-1;
    var textFieldQuestions = document.getElementsByClassName("lp_input-field");
    var radioButtonQuestionArray = document.getElementsByClassName('lp_radiobutton_wrapper');
    var routingQuestion = allQuestions[routingDropDownNumber];
    var textFieldRequiredQuestions = [];
    //    console.log("Qlength="+textFieldQuestions.length);
    for(var i = 0; i < textFieldQuestions.length;i++) {
        console.log("aria="+textFieldQuestions[i].getAttribute("aria-required"));
        if(textFieldQuestions[i].getAttribute("aria-required") === "true"){

            textFieldRequiredQuestions.push(textFieldQuestions[i]);
        }
    }

    /*
     // Find and hide questions based on div put in label
     var hide_fields = document.getElementsByClassName('hide_field_q');

     for(var i = 0;i < hide_fields.length; i++) {
     hide_fields[i].parentNode.parentNode.childNodes[1].setAttribute("style", "display: none !important;");
     }

     var hide_all = document.getElementsByClassName('hide_both_q');

     for(var i = 0;i < hide_all.length; i++) {
     hide_all[i].parentNode.parentNode.childNodes[0].setAttribute("style", "display: none !important;");
     hide_all[i].parentNode.parentNode.childNodes[1].setAttribute("style", "display: none !important;");
     }
     //     console.log("Questions hidden");
     */

    // find real submit button and create fake submit button
    realSubmitButton = document.querySelectorAll("button[data-lp-point='submit_button']")[0];

    //  realSubmitButton.onClick = createLead();


    // hidesurvey = "true";
    if(true) { //Unconditional

        /*change events to elements in PCS*/
        var changeEvent = new lp_CustomEvent('change');
        var clickEvent = new lp_CustomEvent('input');

        /*get question values from SDES on page*/
        var sdes = lpTag.sdes.get();
        var cinfo = sdes.ctmrinfo;
        var pinfo = sdes.personal;
        var customerInfo = [];
        var personalInfo = [];
        if(cinfo !== undefined && cinfo[cinfo.length-1].info !== undefined) {
            customerInfo = lpTag.sdes.get().ctmrinfo[lpTag.sdes.get().ctmrinfo.length - 1].info;

        }
        else {
            console.log('customer info not set');
        }

        if(pinfo !== undefined && pinfo[pinfo.length-1].personal !== undefined) {
            personalInfo = lpTag.sdes.get().personal[lpTag.sdes.get().personal.length - 1].personal;
            globalPersonalInfo = personalInfo;
        }
        else {
            console.log('personal info not set');
        }






        questions = textFieldRequiredQuestions;
        /*get questions from pcs   --  fields are offest by PCS -- where PCD name is index zero, offline name index is 3*/
        console.log("Text field count="+textFieldRequiredQuestions.length);
        if (PCSexists) {
            stateQS = allQuestions[3];
            purposeQS = allQuestions[4];
            nameQS = "";
            nameQS = textFieldRequiredQuestions[3];
            emailQS = textFieldRequiredQuestions[4];
            phoneQS = textFieldRequiredQuestions[5];
        }
        else {
            stateQS = allQuestions[0];
            purposeQS = allQuestions[1];
            nameQS = "";
            nameQS = textFieldRequiredQuestions[0];
            emailQS = textFieldRequiredQuestions[1];
            phoneQS = textFieldRequiredQuestions[2];
        }

        var contactsArr = personalInfo.contacts;

        globalContacts = personalInfo.contacts;

        if(contactsArr === undefined) {
            contactsArr = [];
        }

        // var contacts = contactsArr[0];
        var contacts = globalContacts;
        if(contacts === undefined) {
            contacts = [];
        }

        var name = "";
        var state = "";
        var email = "";
        var phone = "";
        var purpose = "";
        var identifier = "";


        /*get question answers from PCS*/
        if (PCSexists) {
            state = PCSState;
            email = PCSEmail;
            phone = PCSPhone;
            purpose = PCSPurpose;
            identifier = PCSName;
        }
        else {

            console.log("get question answers from sdes on page");
            if (personalInfo != "undefined")   {
                if ( personalInfo.firstname === undefined) {
                    personalInfo.firstname = "";
                }
                if ( personalInfo.lastname === undefined) {
                    personalInfo.lastname = "";
                }
                identifier =  personalInfo.firstname + " " + personalInfo.lastname;
                if (identifier === " ") {
                    identifier = "";
                }

                // var phone = personalInfo.contacts.phone; //Accessing Phone 01-12-2017
                // var email = personalInfo.contacts.email; //accessing phone 01-12-2017
                email = personalInfo.contacts[0].email; // Working
                phone = personalInfo.contacts[0].phone; // Working

            }
            state = customerInfo.accountName;
            //      email = personalInfo.contacts[0].email;
            //       phone = contacts.phone;
            purpose = customerInfo.ctype.charAt(0).toUpperCase() + customerInfo.ctype.slice(1);
        }



        /*type checking answers*/

        if(state === undefined) {
            state = "";
        }
        if(email === undefined) {
            email = "";
        }
        if(phone === undefined) {
            phone = "";
        }
        if(purpose === undefined) {
            purpose = "";
        }

        if(typeof state !== "undefined" && state.length === 2) {
            state = abbrState(state, "name");
        }

        console.log("Offline State="+state);


        /*auto fill questions with answers
         fire events for LE to recognize change
         */
        nameQS.value = identifier;
        nameQS.dispatchEvent(clickEvent);

        emailQS.value = email;
        emailQS.dispatchEvent(clickEvent);

        phoneQS.value = phone.replace(/\D/g,'');
        phoneQS.dispatchEvent(clickEvent);

        var desiredState = state;
        console.log("Desired State="+desiredState);
        for(var i=0; i<stateQS.options.length; i++) {
            if (stateQS.options[i].text == desiredState ) {
                stateQS.selectedIndex = i;
                break;
            }
        }
        stateQS.dispatchEvent(changeEvent);

        var desiredPurpose = purpose;
        for(var i=0; i<purposeQS.options.length; i++) {
            if (purposeQS.options[i].text == desiredPurpose ) {
                purposeQS.selectedIndex = i;
                break;
            }
        }
        purposeQS.dispatchEvent(changeEvent);


        /*checking if all passed
         Case 1) Questions not found
         Case 2) All sdes were not found to auto submit survey, re-show survey
         Case 3) Auto submit PCS
         */
        if(stateQS === "" || purposeQS === "" || nameQS === "" || emailQS === "" || phoneQS === "") {
            console.log('questions not found');
        }
        else if (state === "" || email === ""  || purpose === "" || identifier === ""){
            document.getElementsByClassName('lp_survey_area')[0].setAttribute("style", "display: block !important;")
        }
        else {
            console.log("submitting");
            //      alert("submitting");
            //   fakeSubmitButton.click();
        }
    }

}



function createLead() {

    console.log("Create Lead");

    var allQuestions = document.getElementsByClassName('lp_select_field');
    var routingDropDownNumber = allQuestions.length-1;
    var textFieldQuestions = document.getElementsByClassName("lp_input-field");
    var textComments = document.getElementsByClassName("lp_textarea_field"); //CREATED FOR COMMENTS ONLy (1 single element)
    var radioButtonQuestionArray = document.getElementsByClassName('lp_radiobutton_wrapper');
    var routingQuestion = allQuestions[routingDropDownNumber];
    var textFieldRequiredQuestions = [];
    var comments  = '';
    //var vendorId = '';

    //    console.log("Qlength="+textFieldQuestions.length);
    for(var i = 0; i < textFieldQuestions.length;i++) {
        console.log("aria="+textFieldQuestions[i].getAttribute("aria-required"));
        if(textFieldQuestions[i].getAttribute("aria-required") === "true"){

            textFieldRequiredQuestions.push(textFieldQuestions[i]);
        }
    }
    // Find and hide questions based on div put in label
    /*
     var hide_fields = document.getElementsByClassName('hide_field_q');

     for(var i = 0;i < hide_fields.length; i++) {
     hide_fields[i].parentNode.parentNode.childNodes[1].setAttribute("style", "display: none !important;");
     }

     var hide_all = document.getElementsByClassName('hide_both_q');

     for(var i = 0;i < hide_all.length; i++) {
     hide_all[i].parentNode.parentNode.childNodes[0].setAttribute("style", "display: none !important;");
     hide_all[i].parentNode.parentNode.childNodes[1].setAttribute("style", "display: none !important;");
     }
     */

    //getting the vendor ID Value from LPCart.products
    //	for (var i = 0; i < LPCart.products.length; i++) {
    //		if (LPCart.products[i].product.name == "vendor ID") {
    //			vendorId = LPCart.products[i].product.category;
    //		}
    //	}

    /*get questions from pcs   --  fields are offset by PCS -- where PCD name is index zero, offline name index is 2*/
    if (preChat) {
        console.log("Has prechat");
        stateQS = allQuestions[3];
        purposeQS = allQuestions[4];
        nameQS = "";
        nameQS = textFieldRequiredQuestions[3];

        questions = textFieldRequiredQuestions;
        emailQS = textFieldRequiredQuestions[4];
        phoneQS = textFieldRequiredQuestions[5];
        comments  = textComments[0]; //receiving TextArea Object for Comments
    }
    else {
        console.log("No prechat");
        stateQS = allQuestions[0];
        purposeQS = allQuestions[1];
        nameQS = "";
        nameQS = textFieldRequiredQuestions[0];

        questions = textFieldRequiredQuestions;
        emailQS = textFieldRequiredQuestions[1];
        phoneQS = textFieldRequiredQuestions[2];

        comments  = textComments[0]; //receiving TextArea Object for Comments
    }

    /*
     var state = PCSState;
     var email = PCSEmail;
     var phone = PCSPhone;
     var purpose = PCSPurpose;
     var identifier = PCSName;
     */
    var name = nameQS.value;
    console.log("Name="+nameQS+";");
    var email = emailQS.value;
    console.log("email="+emailQS+";");
    var state = stateQS[stateQS.selectedIndex].text;
    console.log("State="+stateQS+";");
    var comment = ""; // comments added to be placed in to queryString
    var comment = comments.value;
    console.log("Comments="+comment)

    //	 console.log("vendorid="+vendorId);
    var comment = comments.value; // comments added to be placed in to queryString
    var purpose = purposeQS[purposeQS.selectedIndex].text;
    var phone = phoneQS.value;
    var state3PN = abbrState(state, "abbr");
    console.log("Purpose="+purpose);


    if(state === "Choose item from the list" || purpose === "Choose item from the list" || name === "" || email === "" || phone === "") {
        console.log('Lead not created -- some questions missing');
        return;
    }

    // Send answers to LOLA using
    // var send3PN = function (queryString, employeeId, type) {
    queryString = "identifier="+encodeURIComponent(name)+"&phone=" + encodeURIComponent(phone);
    queryString += '&purpose='+purpose+'&email='+encodeURIComponent(email);
    queryString += '&state='+encodeURIComponent(state3PN)+'&name='+encodeURIComponent(name);
    queryString += '&comments='+encodeURIComponent(comment);
    // queryString += '&vendorid='+encodeURIComponent(vendorId);

    var url = "https://ts.liveperson.com/widget/sandbox/test-app15.php";
    url += "?cmd=3&" + queryString;

    console.log("URL="+url);


    $.ajax({
        url: url,
        type: "GET",
        success: function(data, textStatus, xhr){
            console.log("3PN success - " + xhr.status);
            leadsent=true;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status + " - " + thrownError);
            log_error("send3PN function failed in ajax call");
        }
    });




}
